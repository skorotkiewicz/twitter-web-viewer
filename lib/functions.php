<?php

  require_once('config.php');

  $json = file_get_contents('http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . 'twitter-json.php');
  $statuses = json_decode($json, true);
  $profil   =  $statuses['author'];
  $statuses =  $statuses['items'];

  $dark_theme ? $tstyle = 'dark' : $tstyle = 'light';
  $dark_theme ? $bstyle = 'style="background-color: #000 !important;"' : $bstyle = '';
  $avatar ? $tavatar = $profil['avatar'] : $tavatar = 'styles/twitter.svg';

  function retweet($tweet){
    $tweet = preg_replace('#[\r\n]+#', "\n", $tweet);
    $tweet = nl2br($tweet);
    return preg_replace('/^RT /', '<div class="retweeted badge badge-primary">Retweet</div>', $tweet, 2);
  }

  function parse_tweet($text){

    //users
    $text = preg_replace(
            '/@(\w+)/',
            '<a href="http://twitter.com/$1">@$1</a>',
            $text);

    //hashtags
    $text = preg_replace(
            '/\s+#(\w+)/',
            ' <a href="http://twitter.com/search?q=%23$1">#$1</a>',
            $text);

    return $text;

}