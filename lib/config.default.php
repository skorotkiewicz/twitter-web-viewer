<?php

require_once 'twitter-api/twitter.class.php';

// enables caching (path must exists and must be writable!)
//Twitter::$cacheDir = __DIR__ . '/temp';

$consumerKey = '';
$consumerSecret = '';
$accessToken = '';
$accessTokenSecret = '';
  
$screen_name = '';
$full_name = '';
$amountTweets = 20;
$dark_theme = true; // true for dark theme
$avatar = false; // true for your avatar instead of the twitter logo

$tweetPics = true; // if true then show images in tweet
$allTweetPics = true; // if true then show all images in tweet otherwise show only one image