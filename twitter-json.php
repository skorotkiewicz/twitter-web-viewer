<?php

  require_once('lib/config.php');

  $twitter = new Twitter($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
  $statuses = $twitter->load(Twitter::ME | Twitter::RETWEETS, $amountTweets);
  $folder = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/') + 1);
  $url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];

  function imagehw($t) {
    return array('image' => $t->media_url_https, 'thumb' => array( 'h' => $t->sizes->thumb->h, 'w' => $t->sizes->thumb->w ) );
  }

  $feed = [
    'version'       => 'https://jsonfeed.org/version/1',
    'title'         => 'Twitter - ' . $full_name . ' / Timeline',
    'home_page_url' => $url . $folder,
    'feed_url'      => $url . $_SERVER['REQUEST_URI'],
    'icon'          => $url . $folder . 'styles/apple-touch-icon-192x192.png',
    'favicon'       => $url . $folder . 'styles/favicon.ico',
    'author'          => array(
      'name'            => $full_name,
      'url'             => 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'],
      'avatar'          => $statuses[0]->user->profile_image_url_https,
      'statuses_count'  => $statuses[0]->user->statuses_count,
      'followers_count' => $statuses[0]->user->followers_count,
      'friends_count'   => $statuses[0]->user->friends_count),
    'items'         => [],
  ];

  foreach ($statuses as $status) {
    $mediaRT = array();
    $mediaEN = array();

    $tweetText = Twitter::jsonviewer($status);

    $tweet    = !empty($status->retweeted_status->full_text) ? $status->retweeted_status->full_text : $status->full_text;
    
    //$tweetPics = true; // if true then show images in tweet
    //$allTweetPics = true; // if true then show all images in tweet otherwise show only one image

    if ( $tweetPics ) {

      if ( !empty($status->retweeted_status->entities->media) OR !empty($status->entities->media) ) {

        if ( $allTweetPics ) {

            foreach ( $status->retweeted_status->extended_entities->media as $photo ) {
              $mediaRT[] = imagehw($photo);
            }

            foreach ( $status->extended_entities->media as $photo ) {
              $mediaEN[] = imagehw($photo);
            }
          
        } else {

          $rt = $status->retweeted_status->entities->media[0];    
          $en = $status->entities->media[0];

          $mediaRT[] = imagehw($rt);
          $mediaEN[] = imagehw($en);
          
        }

      }

    }

    $media    = (!empty($mediaRT) ? $mediaRT : (!empty($mediaEN) ? $mediaEN : NULL));
    $statusRT = !empty($tweetText['retweeted_status']) ? 'RT' : NULL;

    $feed['items'][] = [
      'id'             => $status->id,
      'date_published' => $status->created_at,
      'content_text'   => $tweetText['tweet'],
      'retweeted'      => $statusRT,
      'media'          => $media,
      'source'         => $status->source,
    ];

  }

  header('Content-Type: application/json');
  echo json_encode($feed, JSON_PRETTY_PRINT);

?>