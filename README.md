# Twitter-web-viewer
Personalized twitter viewer for your webpage written in PHP

## How to Install

1. Get Source Code
```bash
$ git clone https://gitlab.itunix.eu/skorotkiewicz/twitter-web-viewer
```

2. Install Composer

3. Get into source code folder and install packages with composer
```bash
$ cd twitter-web-viewer
$ composer install
```

4. Prepare your own config, copy config template and file it correctly 
```bash
$ cd lib/
$ mv config.default.php config.php
```

5. Add permission to write for your http server
```bash
chown -R www-data twitter-web-viewer/
```

