<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{$full_name} (@{$screen_name}) - Twitter Timeline">
    <link rel="shortcut icon" href="styles/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="styles/apple-touch-icon-192x192.png" sizes="192x192">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="styles/offcanvas.css" rel="stylesheet">
    <meta name="author" content="{$full_name}">
    <title>Twitter Timeline - {$full_name}</title>
    <style>
        body { 
            background-image: url(https://storage.googleapis.com/itunix-static/twitterbg.jpg);
            background-position: center center;
            background-repeat:  no-repeat;
            background-attachment: fixed;
            background-size:  cover;
            background-color: #999;
        }
    </style>
  </head>

  <body class="bg-<?=$tstyle?>">

    <main role="main" class="container">
      <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-grey-dark rounded box-shadow" {$bstyle}>   
        <img class="mr-3 rounded" src="{$tavatar}" alt="" width="48" height="48">
        <div class="lh-100">
          <h6 class="mb-0 text-white lh-100">{$full_name} (@{$screen_name})</h6>
          <small>Twitter Timeline</small>
        </div>
      </div>

      <div class="nav-scroller bg-white box-shadow">
        <nav class="nav nav-underline">
          <a class="nav-link active" href="https://twitter.com/{$screen_name}">Timeline</a>
          <a class="nav-link" href="https://twitter.com/{$screen_name}">Tweets <span class="badge badge-pill bg-light align-text-bottom">{$profil_statuses_count}</span></a>
          <a class="nav-link" href="https://twitter.com/{$screen_name}/followers">Followers <span class="badge badge-pill bg-light align-text-bottom">{$profil_followers_count}</span></a>
          <a class="nav-link" href="https://twitter.com/{$screen_name}/following">Following <span class="badge badge-pill bg-light align-text-bottom">{$profil_friends_count}</span></a>
          <a class="nav-link" href="twitter-json.php">JSON Feed</a>
        </nav>
      </div>

      <div class="my-3 p-3 bg-light rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Recent updates</h6>
	<br />

        {foreach $statuses status}
        <div class="col-md-12">
            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-200 position-relative">
              <div class="col p-4 d-flex flex-column position-static">
                <div class="mb-1 text-muted tweetid"><a href="https://twitter.com/{$screen_name}/status/{$status.id}">{$status.date_published}</a> <span style="float: right;">via {$status.source}</span></div>
                <p class="card-text mb-auto">{$status.content_text}</p>
              </div>
              {if $status.media}
                <div class="col-auto d-none d-lg-block">
                  {foreach $status.media photo}
                    <a href="{$photo.image}" target="_blank">
                        <img src="{$photo.image}" class="rounded" style="object-fit: cover; margin: 15px; width: {$photo.thumb.w}px; height: {$photo.thumb.h}px; " />
                    </a>
                  {/foreach}
                </div>
              {/if}
            </div>
        </div>
        {/foreach}

      </div>

      <div class="alert alert-light" role="alert">
        <tt>
          made with ❤︎ by <a href="https://twitter.com/skorotkiewicz">skorotkiewicz</a></small> / source code <a href="https://gitlab.itunix.eu/skorotkiewicz/twitter-web-viewer">here</a> <br />
          and here is my <a href="/lastfm/">music log</a>
        </tt>
      </div>
      <div class="alert alert-warning" role="alert">
        Have you found an bug or have suggestions? Write it to me! <br /> <a href="mailto:admin@itunix.eu">admin at itunix dot eu</a>
      </div>
      
    </main>

  </body>
</html>