<?php

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/lib/functions.php');

$dwoo = new Dwoo\Core();
$tpl = new Dwoo\Template\File(__DIR__ . '/templates/index.tpl');

$data = new Dwoo\Data();

$data->assign('tavatar', $tavatar);
$data->assign('bstyle', $bstyle);

$data->assign('profil_statuses_count', $profil['statuses_count']);
$data->assign('profil_followers_count', $profil['followers_count']);
$data->assign('profil_friends_count', $profil['friends_count']);

$data->assign('full_name', $full_name);
$data->assign('screen_name', $screen_name);

foreach($statuses as $status) {
    $s[] = array(
            'id' => $status['id'],
            'source' => $status['source'],
            'date_published' => date("j M H:i",strtotime($status['date_published'])),
            'content_text' => retweet(parse_tweet($status['content_text'])),
            'media' => $status['media']                
    );
}

$data->assign('statuses', $s);

echo $dwoo->get($tpl, $data);

?>